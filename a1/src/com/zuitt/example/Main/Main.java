package com.zuitt.example.Main;

import java.util.ArrayList;

public class Main {
    public static  void main (String[] args){

        Phonebook phonebook = new Phonebook();

        Contact newContact1 = new Contact();
        newContact1.setName("John Doe");
        newContact1.setContactNumber("+639152468596");
        newContact1.setAddress("Quezon City");

        Contact newContact2 = new Contact();
        newContact2.setName("Jane Doe");
        newContact2.setContactNumber("+6391621248573");
        newContact2.setAddress("Caloocan City");


        phonebook.setContacts(newContact1);
        phonebook.setContacts(newContact2);

        phonebook.showPhonebook();


    }
}
