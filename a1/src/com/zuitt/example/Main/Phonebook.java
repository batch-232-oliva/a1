package com.zuitt.example.Main;

import java.util.ArrayList;

public class Phonebook {
    // Property
    private ArrayList<Contact> contacts = new ArrayList<>();

    // Empty Constructor
    public Phonebook(){};

    // Parameterized Constructor
    public Phonebook(Contact contact){
        this.contacts.add(contact);
    }


    // Getter
    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    // Setter
    public void setContacts(Contact contact){
        this.contacts.add(contact);
    }

    // Method
    public void showPhonebook(){
        if (this.contacts.size() == 0){
            System.out.println("Your Phonebook is empty!");
        } else {
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("---------------------------------------");
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("my home in " + contact.getAddress());
                System.out.println(" ");
            }
        }
    }
}
